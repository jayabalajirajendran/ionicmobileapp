// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDpZ8vpAbUGUlCcHEu_nlgr3o1ENashdrQ",
    authDomain: "discountwall-a088b.firebaseapp.com",
    databaseURL: "https://discountwall-a088b.firebaseio.com",
    projectId: "discountwall-a088b",
    storageBucket: "discountwall-a088b.appspot.com",
    messagingSenderId: "327076360280",
    appId: "1:327076360280:web:6ae49f111e15f90918da2d",
    measurementId: "G-28JWHTHV82"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
