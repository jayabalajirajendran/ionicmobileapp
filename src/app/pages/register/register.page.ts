import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AuthenticationService } from '../../services/authentication.service';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { UserDetails } from '../../services/user.details.service';
import { User } from '../../../shared/user'

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {


  validations_form: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };
  allet: any;
  alert: any;

  constructor(
    private navCtrl: NavController,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private alertController: AlertController,
    public loadingController: LoadingController,
    public userService: UserDetails,
    public afStore: AngularFirestore
  ) { }

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ]))
    });
  }

  async tryRegister(value: { email: string; password: string }) {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      duration: 1500
    });
    await loading.present();
    var name = (<HTMLInputElement>document.getElementById('name')).value;
    this.authService.registerUser(value)
      .then(res => {
        console.log(res);
        var userUpdate = this.authService.currentUserDetails();
        var object=this;
        userUpdate.updateProfile({ displayName: name, photoURL: 'photo' }).then(function () {
          console.log(userUpdate)
          const userRef: AngularFirestoreDocument<any> = object.afStore.doc(`users/${userUpdate.uid}`);
          const userData: User = {
            uid: userUpdate.uid,
            email: userUpdate.email,
            displayName: userUpdate.displayName,
            photoURL: userUpdate.photoURL,
            emailVerified: userUpdate.emailVerified
          }
          object.presentAlertConfirm("Successfull", "Your account has been created. Please log in.")
          return userRef.set(userData, {
            merge: true
          })
        })
      }, err => {
        console.log(err);
        this.errorMessage = "The email address is already in use";
        this.successMessage = "";
      })
  }

  goLoginPage() {
    this.navCtrl.navigateBack('/login');
  }

  async presentAlertConfirm(header: string, message: string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            this.navCtrl.navigateForward("/home", { replaceUrl: true });
          }
        }
      ]
    });

    await alert.present();
  }


}