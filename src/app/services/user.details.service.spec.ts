import { TestBed } from '@angular/core/testing';

import { UserDetails } from './user.details.service';

describe('UserDetails', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserDetails = TestBed.get(UserDetails);
    expect(service).toBeTruthy();
  });
});
