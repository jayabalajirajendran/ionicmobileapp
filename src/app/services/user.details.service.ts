import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { User } from '../../shared/user';

 
@Injectable({
  providedIn: 'root'
})
export class UserDetails {
  private users: Observable<User[]>;
  private userCollection: AngularFirestoreCollection<User>;
 
  constructor(private afs: AngularFirestore) {
    this.userCollection = this.afs.collection<User>('users');
    this.users = this.userCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
 
  getAllUsers(): Observable<User[]> {
    return this.users;
  }
 
  getUser(uid: string): Observable<User> {
    return this.userCollection.doc<User>(uid).valueChanges().pipe(
      take(1),
      map(user => {
        user.uid = uid;
        return user
      })
    );
  }
 
  addUserDoc(user: User): Promise<DocumentReference> {
    return this.userCollection.add(user);
  }
 
  updateDisplayName(user: User): Promise<void> {
    return this.userCollection.doc(user.uid).update({ displayName: user.displayName});
  }

  updatePhotoURL(user: User): Promise<void> {
    return this.userCollection.doc(user.uid).update({ photoURL: user.photoURL});
  }
 
  deleteUserDoc(uid: string): Promise<void> {
    return this.userCollection.doc(uid).delete();
  }
}