import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActionSheetController,NavController, AlertController, LoadingController  } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-folder',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public title: string;
  public movieList = [];
  public homePage: any;
  public actionPage: any;
  public userData: any;
  sliderConfig = {
    slidesPerView: 1.6,
    spaceBetween: 0.2,
    centeredSlides: true
  };
  displayName: string;
  userName: any;


  constructor(private activatedRoute: ActivatedRoute,private navCtrl: NavController, public actionSheetController: ActionSheetController, private camera: Camera,
    private route: Router, public authService: AuthenticationService,
    private alertController: AlertController,
    public loadingController: LoadingController) { }



  ngOnInit() {
    if (this.authService.currentUserDetails()) {
      this.userName = this.authService.currentUserDetails().displayName
      console.log(this.userName)
    }
    this.movieList = this.getProducts();
    this.homePage = [
      { title: "", body: "", picture: "https://media1.popsugar-assets.com/files/2010/12/50/1/192/1922283/e1b511ab7f3373bf_2010-action-movies.jpg" },
      { title: "", body: "", picture: "https://media1.popsugar-assets.com/files/2010/12/49/2/192/1922283/17a76c96f0a57af9_drama-movie.jpg" },
      { title: "", body: "", picture: "https://media1.popsugar-assets.com/files/thumbor/PkKCDYWvSjTfu1jlarIBLKrbpxA/fit-in/550x550/filters:format_auto-!!-:strip_icc-!!-/2011/12/51/1/192/1922283/90984b3042e07cb0_DRAMA.jpg" },
      { title: "", body: "", picture: "https://media1.popsugar-assets.com/files/2011/07/30/4/192/1922283/7638fecca692a4d8_MonthlyMoviePoll.jpg" }
    ];
  }




  public data = [
    {
      category: 'Action',
      title: 'Action Movies',
      expanded: true,
      movie_1: [
        { id: 'tt4154796', name: 'Avengers - Endgame', image: 'https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_SX300.jpg' },
        { id: 'tt5433138', name: 'F9', image: 'https://m.media-amazon.com/images/M/MV5BZDZlMjc0MTItZWY2YS00ZDJhLWI2M2MtODVjNWFmYTQxYmJjXkEyXkFqcGdeQXVyMjMxOTE0ODA@._V1_SX300.jpg' },
        { id: 'tt6146586', name: 'John Wick: Chapter 3 - Parabellum', image: 'https://m.media-amazon.com/images/M/MV5BMDg2YzI0ODctYjliMy00NTU0LTkxODYtYTNkNjQwMzVmOTcxXkEyXkFqcGdeQXVyNjg2NjQwMDQ@._V1_SX300.jpg' },
        { id: 'tt4154664', name: 'Captain Marvel', image: 'https://m.media-amazon.com/images/M/MV5BMTE0YWFmOTMtYTU2ZS00ZTIxLWE3OTEtYTNiYzBkZjViZThiXkEyXkFqcGdeQXVyODMzMzQ4OTI@._V1_SX300.jpg' },
        { id: 'tt1386703', name: 'Total Recall', image: 'https://m.media-amazon.com/images/M/MV5BN2ZiMDMzYWItNDllZC00ZmRmLWI1YzktM2M5M2ZmZDg1OGNlXkEyXkFqcGdeQXVyNDQ2MTMzODA@._V1_SX300.jpg' },
        { id: 'tt1745960', name: 'Top Gun: Maverick', image: 'https://m.media-amazon.com/images/M/MV5BNTEyYTA5YWYtYmIxYS00NWRlLWExNjMtNjliZmVlZDgxNTBlXkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_SX300.jpg' },
        { id: 'tt1634106', name: 'Bloodshot', image: 'https://m.media-amazon.com/images/M/MV5BYjA5YjA2YjUtMGRlNi00ZTU4LThhZmMtNDc0OTg4ZWExZjI3XkEyXkFqcGdeQXVyNjUyNjI3NzU@._V1_SX300.jpg' },
        { id: 'tt3480822', name: 'Black Widow', image: 'https://m.media-amazon.com/images/M/MV5BZGRlNTY3NGYtM2YzZS00N2YyLTg0ZDYtNmY2ZDg2NDM3N2JlXkEyXkFqcGdeQXVyNTI4MzE4MDU@._V1_SX300.jpg' }
      ]
    },
    {
      category: 'Series',
      title: 'TV Series',
      expanded: true,
      movie_1: [
        { id: 'tt6468322', name: 'Money Heist', image: 'https://m.media-amazon.com/images/M/MV5BZDcxOGI0MDYtNTc5NS00NDUzLWFkOTItNDIxZjI0OTllNTljXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg' },
        { id: 'tt0944947', name: 'Game of Thrones', image: 'https://m.media-amazon.com/images/M/MV5BMjA5NzA5NjMwNl5BMl5BanBnXkFtZTgwNjg2OTk2NzM@._V1_SX300.jpg' },
        { id: 'tt0903747', name: 'Breaking Bad', image: 'https://m.media-amazon.com/images/M/MV5BMjhiMzgxZTctNDc1Ni00OTIxLTlhMTYtZTA3ZWFkODRkNmE2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg' },
        { id: 'tt4574334', name: 'Stranger Things', image: 'https://m.media-amazon.com/images/M/MV5BZGExYjQzNTQtNGNhMi00YmY1LTlhY2MtMTRjODg3MjU4YTAyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg' },
        { id: 'tt2085059', name: 'Black Mirror', image: 'https://m.media-amazon.com/images/M/MV5BYTM3YWVhMDMtNjczMy00NGEyLWJhZDctYjNhMTRkNDE0ZTI1XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg' },
        { id: 'tt1520211', name: 'The Walking Dead', image: 'https://m.media-amazon.com/images/M/MV5BYTUwOTM3ZGUtMDZiNy00M2I3LWI1ZWEtYzhhNGMyZjI3MjBmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg' },
        { id: 'tt2442560', name: 'Peaky Blinders', image: 'https://m.media-amazon.com/images/M/MV5BMTkzNjEzMDEzMF5BMl5BanBnXkFtZTgwMDI0MjE4MjE@._V1_SX300.jpg' },
        { id: 'tt4786824', name: 'The Crown', image: 'https://m.media-amazon.com/images/M/MV5BNGI1ODkzZDQtZTYxYS00MTg1LWFlY2QtMTM5MGNhNWRhYmVmXkEyXkFqcGdeQXVyNjU2ODM5MjU@._V1_SX300.jpg' }
      ]
    },
    {
      category: 'Romantic',
      title: 'Romantic Movies',
      expanded: true,
      movie_1: [
        { id: 'tt0120338', name: 'Titanic', image: 'https://m.media-amazon.com/images/M/MV5BMDdmZGU3NDQtY2E5My00ZTliLWIzOTUtMTY4ZGI1YjdiNjk3XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SX300.jpg' },
        { id: 'tt1517451', name: 'A Star is Born', image: 'https://m.media-amazon.com/images/M/MV5BNmE5ZmE3OGItNTdlNC00YmMxLWEzNjctYzAwOGQ5ODg0OTI0XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg' },
        { id: 'tt3104988', name: 'Crazy Rich Asians', image: 'https://m.media-amazon.com/images/M/MV5BMTYxNDMyOTAxN15BMl5BanBnXkFtZTgwMDg1ODYzNTM@._V1_SX300.jpg' },
        { id: 'tt1714206', name: 'The Spectacular Now', image: 'https://m.media-amazon.com/images/M/MV5BMjA5MTc0NTkzM15BMl5BanBnXkFtZTcwODEwNjE3OQ@@._V1_SX300.jpg' },
        { id: 'tt2582846', name: 'The Fault in Our Stars', image: 'https://m.media-amazon.com/images/M/MV5BMjA4NzkxNzc5Ml5BMl5BanBnXkFtZTgwNzQ3OTMxMTE@._V1_SX300.jpg' },
        { id: 'tt5726616', name: 'Call Me by Your Name', image: 'https://m.media-amazon.com/images/M/MV5BNDk3NTEwNjc0MV5BMl5BanBnXkFtZTgwNzYxNTMwMzI@._V1_SX300.jpg' },
        { id: 'tt2194499', name: 'About Time', image: 'https://m.media-amazon.com/images/M/MV5BMTA1ODUzMDA3NzFeQTJeQWpwZ15BbWU3MDgxMTYxNTk@._V1_SX300.jpg' },
        { id: 'tt1045658', name: 'Silver Linings Playbook', image: 'https://m.media-amazon.com/images/M/MV5BMTM2MTI5NzA3MF5BMl5BanBnXkFtZTcwODExNTc0OA@@._V1_SX300.jpg' }
      ]
    },
    {
      category: 'Thriller',
      title: 'Thriller Movies',
      expanded: true,
      movie_1: [
        { id: 'tt0499549', name: 'Avatar', image: 'https://m.media-amazon.com/images/M/MV5BMTYwOTEwNjAzMl5BMl5BanBnXkFtZTcwODc5MTUwMw@@._V1_SX300.jpg' },
        { id: 'tt7286456', name: 'Joker', image: 'https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg' },
        { id: 'tt6644200', name: 'A Quiet Place', image: 'https://m.media-amazon.com/images/M/MV5BMjI0MDMzNTQ0M15BMl5BanBnXkFtZTgwMTM5NzM3NDM@._V1_SX300.jpg' },
        { id: 'tt1051906', name: 'The Invisible Man', image: 'https://m.media-amazon.com/images/M/MV5BZjFhM2I4ZDYtZWMwNC00NTYzLWE3MDgtNjgxYmM3ZWMxYmVmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg' },
        { id: 'tt2872718', name: 'Nightcrawler', image: 'https://m.media-amazon.com/images/M/MV5BN2U1YzdhYWMtZWUzMi00OWI1LWFkM2ItNWVjM2YxMGQ2MmNhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg' },
        { id: 'tt2267998', name: 'Gone Girl', image: 'https://m.media-amazon.com/images/M/MV5BMTk0MDQ3MzAzOV5BMl5BanBnXkFtZTgwNzU1NzE3MjE@._V1_SX300.jpg' },
        { id: 'tt1375666', name: 'Inception', image: 'https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SX300.jpg' },
        { id: 'tt1392214', name: 'Prisoners', image: 'https://m.media-amazon.com/images/M/MV5BMTg0NTIzMjQ1NV5BMl5BanBnXkFtZTcwNDc3MzM5OQ@@._V1_SX300.jpg' }
      ]
    }, {
      category: 'Comedy',
      title: 'Comedy Movies',
      expanded: true,
      movie_1: [
        { id: 'tt2584384', name: 'JoJo Rabbit', image: 'https://m.media-amazon.com/images/M/MV5BZjU0Yzk2MzEtMjAzYy00MzY0LTg2YmItM2RkNzdkY2ZhN2JkXkEyXkFqcGdeQXVyNDg4NjY5OTQ@._V1_SX300.jpg' },
        { id: 'tt1411697', name: 'The Hangover Part II', image: 'https://m.media-amazon.com/images/M/MV5BMTM2MTM4MzY2OV5BMl5BanBnXkFtZTcwNjQ3NzI4NA@@._V1_SX300.jpg' },
        { id: 'tt1232829', name: '21 Jump Street', image: 'https://m.media-amazon.com/images/M/MV5BNTZjNzRjMTMtZDMzNy00Y2ZjLTg0OTAtZjVhNzYyZmJjOTljXkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_SX300.jpg' },
        { id: 'tt1489887', name: 'Booksmart', image: 'https://m.media-amazon.com/images/M/MV5BMjEzMjcxNjA2Nl5BMl5BanBnXkFtZTgwMjAxMDM2NzM@._V1_SX300.jpg' },
        { id: 'tt1478338', name: 'Bridesmaids', image: 'https://m.media-amazon.com/images/M/MV5BMjAyOTMyMzUxNl5BMl5BanBnXkFtZTcwODI4MzE0NA@@._V1_SX300.jpg' },
        { id: 'tt1386588', name: 'The Other Guys', image: 'https://m.media-amazon.com/images/M/MV5BMTc0NDQzNTA2Ml5BMl5BanBnXkFtZTcwNzI2OTQzMw@@._V1_SX300.jpg' },
        { id: 'tt0838283', name: 'Step Brothers', image: 'https://m.media-amazon.com/images/M/MV5BODViZDg3ZjYtMzhiYS00YTVkLTk4MzktYWUxMTlkYjc1NjdlXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg' },
        { id: 'tt0942385', name: 'Tropic Thunder', image: 'https://m.media-amazon.com/images/M/MV5BNDE5NjQzMDkzOF5BMl5BanBnXkFtZTcwODI3ODI3MQ@@._V1_SX300.jpg' },
      ]
    }, {
      category: 'Animation',
      title: 'Animation Movies',
      expanded: true,
      movie_1: [
        { id: 'tt6105098', name: 'The Lion King', image: 'https://m.media-amazon.com/images/M/MV5BMjIwMjE1Nzc4NV5BMl5BanBnXkFtZTgwNDg4OTA1NzM@._V1_SX300.jpg' },
        { id: 'tt4520988', name: 'Frozen II', image: 'https://m.media-amazon.com/images/M/MV5BMjA0YjYyZGMtN2U0Ni00YmY4LWJkZTItYTMyMjY3NGYyMTJkXkEyXkFqcGdeQXVyNDg4NjY5OTQ@._V1_SX300.jpg' },
        { id: 'tt1979376', name: 'Toy Story 4', image: 'https://m.media-amazon.com/images/M/MV5BMTYzMDM4NzkxOV5BMl5BanBnXkFtZTgwNzM1Mzg2NzM@._V1_SX300.jpg' },
        { id: 'tt2245084', name: 'Big Hero 6', image: 'https://m.media-amazon.com/images/M/MV5BMDliOTIzNmUtOTllOC00NDU3LWFiNjYtMGM0NDc1YTMxNjYxXkEyXkFqcGdeQXVyNTM3NzExMDQ@._V1_SX300.jpg' },
        { id: 'tt2380307', name: 'Coco', image: 'https://m.media-amazon.com/images/M/MV5BYjQ5NjM0Y2YtNjZkNC00ZDhkLWJjMWItN2QyNzFkMDE3ZjAxXkEyXkFqcGdeQXVyODIxMzk5NjA@._V1_SX300.jpg' },
        { id: 'tt2277860', name: 'Finding Dory', image: 'https://m.media-amazon.com/images/M/MV5BNzg4MjM2NDQ4MV5BMl5BanBnXkFtZTgwMzk3MTgyODE@._V1_SX300.jpg' },
        { id: 'tt2948356', name: 'Zootopia', image: 'https://m.media-amazon.com/images/M/MV5BOTMyMjEyNzIzMV5BMl5BanBnXkFtZTgwNzIyNjU0NzE@._V1_SX300.jpg' },
        { id: 'tt2096673', name: 'inside out', image: 'https://m.media-amazon.com/images/M/MV5BOTgxMDQwMDk0OF5BMl5BanBnXkFtZTgwNjU5OTg2NDE@._V1_SX300.jpg' }

      ]
    }
  ];
  getProducts() {
    return this.data;
  }
  public takePicture(pictureSource: number) {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      quality: 100,
      correctOrientation: true,
      sourceType: pictureSource
    }).then((imageData) => {
      // imageData is a base64 encoded string
      let base64Image = "data:image/jpeg;base64," + imageData;

      // Variable to select the last picture taken by the ID
      let cameraImageSelector = document.getElementById('camera-image');
      cameraImageSelector.setAttribute('src', base64Image);
    }, (err) => {
      alert(err);
    });
  }

  logoutUser() {
    return new Promise((resolve, reject) => {
      if (firebase.auth().currentUser) {
        firebase.auth().signOut()
          .then(() => {
            this.navCtrl.navigateRoot('/login');
          }).catch((error) => {
            reject();
          });
      }
    })
  }

  login() {
    this.navCtrl.navigateForward('/login');
  }

  async presentAlertConfirm(header: string, message: string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            this.navCtrl.navigateForward("", { replaceUrl: true });
          }
        }
      ]
    });

    await alert.present();
  }

}
