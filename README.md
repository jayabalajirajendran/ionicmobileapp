CA BRIEF:

For this assessment you have to build a mobile application for a business of your choice. Your mobile application should be built using several familiar web technologies such as HTML5, CSS and Javascript and then seamlessly deploy it to platforms such as iOS, Android, and Windows Phone. This application should be touch friendly and provide actual functionality which users can take advantage of. You are allowed to use a UI framework like jQuery Mobile, Sencha Touch, Kendo UI or any other platforms discussed in the module.

PROJECT BRIEF:
    DBS Movie Reviews is a hybrid cross platform application which it will shows the ratings and movie plot for any movies released upto now or even the movies gonna be released in future. Not only that, it search based on movies, tv series and episode related the search query. Onces the user choose the movie picture, it will shows a brief explanation about the movie plot, casting members and the current rating for that movie. User can register and login with created email id to add the movies as watchlist. While registerting, user can upload a profile picture which can be taken using camera or selected from file manager. 

PLUGINS USED:
    1) Cordova-plugin-androidx
    2) Cordova-plugin-androidx-adapter
    3) Cordova-plugin-camera
    4) Corodova-plugi-device
    5) Cordova-plugin-enable-multidex
    6) Cordova-plugin-googleplus
    7) Cordova-plugin-ionic-keyboard
    8) Cordova-plugin-ionic-webview
    9) Cordova-plugin-statusbar
   10) Corodova-plugin-whitelist
   11) Corodova-plugi-firebasex

TECHONOLOGY USED:

    * Firebase  - Used as a backend process for authentication and database management.

    * Movie API - The whole project is implemented using a free movie api to fetch all the movie details in a json format using HTTP Request


NOTE:
    * Please build the application using the following command.

         npm install --force
    